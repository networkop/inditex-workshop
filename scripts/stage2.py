#!/usr/bin/env python3

import argparse
import jinja2
from jinja2 import Template
import os
import yaml

def create_configs(device_name, config):
    filename = os.path.join("./configs", f'{device_name}.txt')
    with open(filename, 'w') as f:
        f.write('\n'.join(config))

def create_interfaces(devices):
    template_name = os.path.join("templates", "interface.j2")
    with open(template_name, 'r') as f:
        jnja = f.read()

    t = Template(jnja)

    for root, _, files in os.walk("./models/interfaces"):
        for file_name in files:
            full_path = os.path.join(root, file_name)
            device_name, _ = os.path.splitext(file_name)
            with open(full_path, 'r') as f:
                device_model = yaml.safe_load(f)
                config = t.render(input_model=device_model)
                device_config = devices.get(device_name, list())
                device_config.append(config)

                devices[device_name] = device_config

    return

def create_bgp(devices):
    template_name = os.path.join("templates", "bgp.j2")
    with open(template_name, 'r') as f:
        jnja = f.read()

    t = Template(jnja)

    for root, _, files in os.walk("./models/bgp"):
        for file_name in files:
            full_path = os.path.join(root, file_name)
            device_name, _ = os.path.splitext(file_name)
            with open(full_path, 'r') as f:
                bgp_model = yaml.safe_load(f)
                config = t.render(input_model=bgp_model)
                device_config = devices.get(device_name, list())
                device_config.append(config)

                devices[device_name] = device_config
    
    return  

def create_else(devices):
    template_name = os.path.join("templates", "else.j2")
    with open(template_name, 'r') as f:
        jnja = f.read()

    t = Template(jnja)

    for root, _, files in os.walk("./models/else"):
        for file_name in files:
            full_path = os.path.join(root, file_name)
            device_name, _ = os.path.splitext(file_name)
            with open(full_path, 'r') as f:
                else_model = yaml.safe_load(f)
                config = t.render(input_model=else_model)
                device_config = devices.get(device_name, list())
                device_config.append(config)

                devices[device_name] = device_config

    return  

def main():

    devices = dict()

    create_else(devices)
    create_interfaces(devices)
    create_bgp(devices)

    for device_name, device_config in devices.items():
        create_configs(device_name, device_config)

    return


if __name__ == '__main__':
    main()