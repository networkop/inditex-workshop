#!/usr/bin/env python2

import pyeapi
import os

DEVICE_INVENTORY = {
    "sw-1": "localhost:9000",
    "sw-2": "localhost:9001"
}


def push_configs(device_url, config):
    url, port = device_url.split(':')
    connection = pyeapi.connect(host=url, port=port, username="admin", password="admin")
    try:
        output = connection.execute(["enable", "configure"] + config.split('\n'))
        print(output)
    except:
        print("Failed to connect to device {}, continuing".format(device_url))

def main():

    for root, _, files in os.walk("./configs"):
        for file_name in files:
            full_path = os.path.join(root, file_name)
            device_name, _ = os.path.splitext(file_name)
            device_url = DEVICE_INVENTORY.get(device_name)
            with open(full_path, 'r') as f:
                config = f.read()
                push_configs(device_url, config)


    return


if __name__ == '__main__':
    main()