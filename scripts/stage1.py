#!/usr/bin/env python3

import argparse
import yaml
import os

def validate_request(req):
    version = req.get('version')
    name = req.get('name')
    ports = req.get('ports')
    if version != "v1":
        return "Wrong version"
    elif not name:
        return "Name is not defined"
    elif not ports:
        return "Ports are not defined"

def validate_port(port):
    device = port.get("device")
    interface = port.get("interface")
    access_vlan = port.get("access_vlan")
    if not device:
        return "Device is not defined for port"
    elif not interface:
        return "Interface is not defined for port"
    elif not access_vlan:
        return "Access vlan is not defined"

def process_port(port, data_model, port_descr):
    err = validate_port(port)
    if err:
        return err
    
    device_name = port.get("device")
    if not device_name in data_model:
        data_model[device_name] = dict()

    device_model = data_model[device_name] 

    intf_name = port.get("interface")
    if not intf_name in device_model:
        device_model[intf_name] = dict()

    access_vlan = port.get("access_vlan")
    device_model[intf_name]["vlan"] = access_vlan
    device_model[intf_name]["description"] = port_descr
    device_model[intf_name]["stp_mode"] = "portfast"
    device_model[intf_name]["snmp_trap"] = False

    return None

    
def datamodel_save(dm):
    dir_name = "models/interfaces"
    for device_name,device_model in dm.items():
        file_name = os.path.join(dir_name, f'{device_name}.yml')
        with open(file_name, 'w') as f:
            f.write(yaml.dump(device_model))


def gather_requests(dirname):
    requests = list()

    for root, _, files in os.walk(dirname):
        for req in files:
            req_filename = os.path.join(root, req)
            requests.append(req_filename)
    
    return requests

def main():

    all_requests = gather_requests("requests")

    data_model = dict()

    for req in all_requests:

        with open(req, 'r') as f:
            parsed_request = yaml.safe_load(f)

        err = validate_request(parsed_request)
        if err:
            print(err)
            return 1

        for port in parsed_request.get("ports"):
            port_descr = parsed_request.get("name")
            err = process_port(port, data_model, port_descr)
            if err:
                print(err)
                return 1

    datamodel_save(data_model)

    return


if __name__ == '__main__':
    main()