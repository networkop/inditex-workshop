FROM alpine

RUN apk update && \
    apk add python3-dev && \
    apk add git && \
    apk add python2-dev && \
    apk add py-pip && \

RUN pip3 install netaddr
RUN pip3 install pyyaml
RUN pip3 install jinja2
RUN pip2 install pyeapi